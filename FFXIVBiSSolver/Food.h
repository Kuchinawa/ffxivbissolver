#pragma once

typedef struct Food
{
    cl_float directHitMod;
    cl_float directHitMax;
    cl_float criticalHitRateMod;
    cl_float criticalHitRateMax;
    cl_float determinationMod;
    cl_float determinationMax;
    cl_float skillSpeedMod;
    cl_float skillSpeedMax;
    cl_float spellSpeedMod;
    cl_float spellSpeedMax;
    cl_float parryMod;
    cl_float parryMax;
    cl_float vitalityMod;
    cl_float vitalityMax;
    cl_float pietyMod;
    cl_float pietyMax;
} Food;