#pragma once

struct GearItem;
struct StatWeights;

class Materia
{
public:
    std::vector<cl_int> allMateriaCombos;

    Materia();
    ~Materia();

    //Encodes the materia combinations and adds them to the combo list
    void AddCombosForItem(GearItem& item, StatWeights statWeights);
    void RemoveCombosForItem(GearItem& item, StatWeights statWeights);

private:
    enum MateriaTypes
    {
        Intelligence,
        DirectHit,
        CriticalHitRate,
        Determination,
        SkillSpeed,
        SpellSpeed,
        Parry,
        Vitality,
        Piety
    };

    const static int materiaTypeCount = 9;

    static MateriaTypes GetMateriaByIndex(int i);
    static int GetMateriaValue(MateriaTypes materiaType);
    //static int GetMateriaIndex(MateriaTypes materiaType);

    int EncryptMateria(std::vector<MateriaTypes> materiaCombo);
    bool OneSlotOverCap(GearItem& item, std::vector<MateriaTypes> materiaCombo);
    float CalcMateriaComboScore(GearItem& item, std::vector<MateriaTypes> materiaCombination, StatWeights statWeights);

    void AddComboPermutation(
        GearItem& item,
        StatWeights statWeights,
        std::vector<MateriaTypes> materiaCombo,
        std::vector<std::vector<MateriaTypes>>& itemMateriaCombos,
        std::vector<float>& itemMateriaComboScores);
};