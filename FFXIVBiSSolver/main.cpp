#include "stdafx.h"
#include "FFXIVBiSSolver.h"
#include <QtWidgets/QApplication>



int main(int argc, char *argv[])
{
#ifdef _DEBUG
    AllocConsole();
    freopen("conin$", "r", stdin);
    freopen("conout$", "w", stdout);
    freopen("conout$", "w", stderr);
    printf("Debugging Window:\n");
#endif

    QApplication a(argc, argv);
    FFXIVBiSSolver w;

    w.show();

    w.InitializeOpenCL();

    return a.exec();
}
