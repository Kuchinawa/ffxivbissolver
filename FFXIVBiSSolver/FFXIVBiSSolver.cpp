#include "stdafx.h"
#include "FFXIVBiSSolver.h"

#include "BaseStats.h"
#include "StatWeights.h"
#include "GearItem.h"
#include "Food.h"
#include "Materia.h"
//#include "GearSet.h"

using namespace std;

int GetSlotCount(std::vector<GearItem> slot);
int WorkSize();

StatWeights statWeights;
BaseStats baseStats;

Materia materia;

vector<GearItem> weapons;
vector<GearItem> offhands;
vector<GearItem> heads;
vector<GearItem> chests;
vector<GearItem> gloves;
vector<GearItem> belts;
vector<GearItem> pants;
vector<GearItem> boots;
vector<GearItem> earrings;
vector<GearItem> necklaces;
vector<GearItem> wrists;
//TODO: Limit unique rings to one of the two
vector<GearItem> ringsL;
vector<GearItem> ringsR;

vector<Food> foods;


FFXIVBiSSolver::FFXIVBiSSolver(QWidget *parent)
    : QWidget(parent)
{
    ui.setupUi(this);
}

void FFXIVBiSSolver::on_solveButton_clicked()
{
    cl_int error;

    statWeights = {
        (cl_float)ui.wdSpinBox->value(),
        (cl_float)ui.mainstatSpinBox->value(),
        (cl_float)ui.dhSpinBox->value(),
        (cl_float)ui.critSpinBox->value(),
        (cl_float)ui.detSpinBox->value(),
        (cl_float)ui.skillSSpinBox->value(),
        (cl_float)ui.spellSSpinBox->value(),
        (cl_float)ui.parrySpinBox->value(),
        (cl_float)ui.vitSpinBox->value(),
        (cl_float)ui.pieSpinBox->value()
    };

    GearItem testItem;

    testItem.weaponDMG = 115;
    testItem.intelligence = 189;
    testItem.directHit = 0;
    testItem.criticalHitRate = 0;
    testItem.determination = 95;
    testItem.skillSpeed = 0;
    testItem.spellSpeed = 142;
    testItem.parry = 0;
    testItem.vitality = 192;
    testItem.piety = 0;

    testItem.intelligenceCap = 0;
    testItem.determinationCap = 142;
    testItem.criticalHitRateCap = 142;
    testItem.determinationCap = 41;
    testItem.skillSpeedCap = 0;
    testItem.spellSpeedCap = 0;
    testItem.parryCap = 0;
    testItem.vitalityCap = 0;
    testItem.pietyCap = 98;

    testItem.materiaSlots = 2;
    testItem.materiaComboIndex = 0;
    testItem.materiaComboCount = 0;

    materia.AddCombosForItem(testItem, statWeights);

    weapons.push_back(testItem);
    offhands.push_back(testItem);
    heads.push_back(testItem);
    chests.push_back(testItem);
    gloves.push_back(testItem);
    belts.push_back(testItem);
    pants.push_back(testItem);
    boots.push_back(testItem);
    earrings.push_back(testItem);
    necklaces.push_back(testItem);
    wrists.push_back(testItem);
    ringsL.push_back(testItem);
    ringsR.push_back(testItem);

    Food testFood;

    testFood.directHitMod = 1.0f;
    testFood.directHitMax = 1.0f;
    testFood.criticalHitRateMod = 1.0f;
    testFood.criticalHitRateMax = 1.0f;
    testFood.determinationMod = 1.0f;
    testFood.determinationMax = 1.0f;
    testFood.skillSpeedMod = 1.0f;
    testFood.skillSpeedMax = 1.0f;
    testFood.spellSpeedMod = 1.0f;
    testFood.spellSpeedMax = 1.0f;
    testFood.parryMod = 1.0f;
    testFood.parryMax = 1.0f;
    testFood.vitalityMod = 1.0f;
    testFood.vitalityMax = 1.0f;
    testFood.pietyMod = 1.0f;
    testFood.pietyMax = 1.0f;

    foods.push_back(testFood);

    baseStats.weaponDMG = 0.0f;
    baseStats.intelligence = 296.0f;
    baseStats.directHit = 354.0f;
    baseStats.criticalHitRate = 354.0f;
    baseStats.determination = 218.0f;
    baseStats.skillSpeed = 354.0f;
    baseStats.spellSpeed = 354.0f;
    baseStats.parry = 354.0f;
    baseStats.vitality = 215.0f;
    baseStats.piety = 230.0f;


    cl_mem_flags readOnlyHPFlag = CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR;

    //Create buffers for kernel argument arrays
    cl_mem weaponsBuffer = clCreateBuffer(CLContext.context, readOnlyHPFlag, sizeof(GearItem) * weapons.size(), weapons.data(), &error);
    cl_mem offhandsBuffer = clCreateBuffer(CLContext.context, readOnlyHPFlag, sizeof(GearItem) * offhands.size(), offhands.data(), &error);
    cl_mem headsBuffer = clCreateBuffer(CLContext.context, readOnlyHPFlag, sizeof(GearItem) * heads.size(), heads.data(), &error);
    cl_mem chestsBuffer = clCreateBuffer(CLContext.context, readOnlyHPFlag, sizeof(GearItem) * chests.size(), chests.data(), &error);
    cl_mem glovesBuffer = clCreateBuffer(CLContext.context, readOnlyHPFlag, sizeof(GearItem) * gloves.size(), gloves.data(), &error);
    cl_mem beltsBuffer = clCreateBuffer(CLContext.context, readOnlyHPFlag, sizeof(GearItem) * belts.size(), belts.data(), &error);
    cl_mem pantsBuffer = clCreateBuffer(CLContext.context, readOnlyHPFlag, sizeof(GearItem) * pants.size(), pants.data(), &error);
    cl_mem bootsBuffer = clCreateBuffer(CLContext.context, readOnlyHPFlag, sizeof(GearItem) * boots.size(), boots.data(), &error);
    cl_mem earringsBuffer = clCreateBuffer(CLContext.context, readOnlyHPFlag, sizeof(GearItem) * earrings.size(), earrings.data(), &error);
    cl_mem necklacesBuffer = clCreateBuffer(CLContext.context, readOnlyHPFlag, sizeof(GearItem) * necklaces.size(), necklaces.data(), &error);
    cl_mem wristsBuffer = clCreateBuffer(CLContext.context, readOnlyHPFlag, sizeof(GearItem) * wrists.size(), wrists.data(), &error);
    cl_mem ringsLBuffer = clCreateBuffer(CLContext.context, readOnlyHPFlag, sizeof(GearItem) * ringsL.size(), ringsL.data(), &error);
    cl_mem ringsRBuffer = clCreateBuffer(CLContext.context, readOnlyHPFlag, sizeof(GearItem) * ringsR.size(), ringsR.data(), &error);

    cl_mem materiaBuffer = clCreateBuffer(CLContext.context, readOnlyHPFlag, sizeof(cl_int) * materia.allMateriaCombos.size(), materia.allMateriaCombos.data(), &error);

    cl_mem foodBuffer = clCreateBuffer(CLContext.context, readOnlyHPFlag, sizeof(Food) * foods.size(), foods.data(), &error);


    error = clSetKernelArg(CLContext.kernel, 0, sizeof(baseStats), &baseStats);
    error = clSetKernelArg(CLContext.kernel, 1, sizeof(statWeights), &statWeights);

    error = clSetKernelArg(CLContext.kernel, 2, sizeof(weaponsBuffer), &weaponsBuffer);
    error = clSetKernelArg(CLContext.kernel, 3, sizeof(offhandsBuffer), &offhandsBuffer);
    error = clSetKernelArg(CLContext.kernel, 4, sizeof(headsBuffer), &headsBuffer);
    error = clSetKernelArg(CLContext.kernel, 5, sizeof(chestsBuffer), &chestsBuffer);
    error = clSetKernelArg(CLContext.kernel, 6, sizeof(glovesBuffer), &chestsBuffer);
    error = clSetKernelArg(CLContext.kernel, 7, sizeof(beltsBuffer), &beltsBuffer);
    error = clSetKernelArg(CLContext.kernel, 8, sizeof(pantsBuffer), &pantsBuffer);
    error = clSetKernelArg(CLContext.kernel, 9, sizeof(bootsBuffer), &bootsBuffer);
    error = clSetKernelArg(CLContext.kernel, 10, sizeof(earringsBuffer), &earringsBuffer);
    error = clSetKernelArg(CLContext.kernel, 11, sizeof(necklacesBuffer), &necklacesBuffer);
    error = clSetKernelArg(CLContext.kernel, 12, sizeof(wristsBuffer), &wristsBuffer);
    error = clSetKernelArg(CLContext.kernel, 13, sizeof(ringsLBuffer), &ringsLBuffer);
    error = clSetKernelArg(CLContext.kernel, 14, sizeof(ringsRBuffer), &ringsRBuffer);

    error = clSetKernelArg(CLContext.kernel, 15, sizeof(materiaBuffer), &materiaBuffer);

    error = clSetKernelArg(CLContext.kernel, 16, sizeof(foodBuffer), &foodBuffer);

    //Worksize
    size_t testDataSize = 1;
    size_t workSize = WorkSize();
    cl_uint globalworksizeDim = 1;
    const size_t globalworksize[] = { testDataSize };

    //Start
    error = clEnqueueNDRangeKernel(CLContext.queue, CLContext.kernel, globalworksizeDim, nullptr, globalworksize, nullptr, 0, nullptr, nullptr);

    //error = clEnqueueReadBuffer(CLContext.queue, statWeightsBuffer, CL_TRUE, 0, sizeof(StatWeights), &statWeights, 0, nullptr, nullptr);

    //Read result
    //clEnqueueReadBuffer(CLContext.queue, CLContext.bBuffer, CL_TRUE, 0, sizeof(float) * testDataSize, b.data(), 0, nullptr, nullptr);



    return;

    //ui.outputText->setPlainText(QString::fromStdString(bestSet.ToString()));
}

void FFXIVBiSSolver::InitializeOpenCL()
{
    CLContext.Initialize(ui.outputText);
}


int WorkSize()
{
    int returnCount = 1;

    std::vector<int> counts =
    {
        GetSlotCount(weapons),
        GetSlotCount(offhands),
        GetSlotCount(heads),
        GetSlotCount(chests),
        GetSlotCount(belts),
        GetSlotCount(pants),
        GetSlotCount(boots),
        GetSlotCount(earrings),
        GetSlotCount(necklaces),
        GetSlotCount(wrists),
        GetSlotCount(ringsL),
        GetSlotCount(ringsR),
        (int)foods.size()
    };

    for (int& n : counts)
    {
        returnCount *= n;
    }

    return returnCount;
}

//Get amount of items and their variations in an equipment slot
int GetSlotCount(std::vector<GearItem> slot)
{
    int count = 0;
    for (GearItem& item : slot)
    {               //materia count or 1 if no materia slots
        count += (item.materiaComboCount > 0) ? item.materiaComboCount : 1;
    }
    return count;
}