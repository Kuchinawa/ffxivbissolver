constant float WEAPD = 0;
constant float MAINSTAT = 1;
constant float DIRECT_HIT = 2;
constant float CRIT = 3;
constant float DETERMINATION = 4;
constant float SKILLSPEED = 5;
constant float SPELLSPEED = 6;
constant float PARRY = 7;
constant float VITALITY = 8;
constant float PIETY = 9;

typedef struct BaseStats
{
    float weaponDMG;
    float intelligence;
    float directHit;
    float criticalHitRate;
    float determination;
    float skillSpeed;
    float spellSpeed;
    float parry;
    float vitality;
    float piety;
} BaseStats;

typedef struct StatWeights
{
    float weaponDMG;
    float intelligence;
    float directHit;
    float criticalHitRate;
    float determination;
    float skillSpeed;
    float spellSpeed;
    float parry;
    float vitality;
    float piety;
} StatWeights;

typedef struct GearItem
{
    float weaponDMG;
    float intelligence;
    float directHit;
    float criticalHitRate;
    float determination;
    float skillSpeed;
    float spellSpeed;
    float parry;
    float vitality;
    float piety;
    float intelligenceCap;
    float directHitCap;
    float criticalHitRateCap;
    float determinationCap;
    float skillSpeedCap;
    float spellSpeedCap;
    float parryCap;
    float vitalityCap;
    float pietyCap;
    int materiaSlots;
    int materiaComboIndex;
    int materiaComboCount;
} GearItem;

typedef struct Food
{
    float directHitMax;
    float directHitMod;
    float criticalHitRateMod;
    float criticalHitRateMax;
    float determinationMod;
    float determinationMax;
    float skillSpeedMod;
    float skillSpeedMax;
    float spellSpeedMod;
    float spellSpeedMax;
    float parryMod;
    float parryMax;
    float vitalityMod;
    float vitalityMax;
    float pietyMod;
    float pietyMax;
} Food;

kernel void Solver(
    BaseStats baseStats,
    StatWeights statWeights,

    global GearItem* weapons,
    global GearItem* offhands,
    global GearItem* heads,
    global GearItem* chests,
    global GearItem* gloves,
    global GearItem* belts,
    global GearItem* pants,
    global GearItem* boots,
    global GearItem* earrings,
    global GearItem* necklaces,
    global GearItem* wrists,
    global GearItem* ringsL,
    global GearItem* ringsR,

    global int* materiaCombos,

    global Food* foods)
{
    const int i = get_global_id(0);

    printf("weaponDMG %f\n", statWeights.weaponDMG);
    printf("intelligence %f\n", statWeights.intelligence);
    printf("directHit %f\n", statWeights.directHit);
    printf("criticalHitRate %f\n", statWeights.criticalHitRate);
    printf("determination %f\n", statWeights.determination);
    printf("skillSpeed %f\n", statWeights.skillSpeed);
    printf("spellSpeed %f\n", statWeights.spellSpeed);
    printf("parry %f\n", statWeights.parry);
    printf("vitality %f\n", statWeights.vitality);
    printf("piety %f\n", statWeights.piety);
}