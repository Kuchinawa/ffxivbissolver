#include "stdafx.h"
#include "OpenCLContext.h"

#include <fstream>
#include <iostream>

using namespace std;

OpenCLContext::OpenCLContext()
{
}


OpenCLContext::~OpenCLContext()
{/* Wait for end of execution and release all resources */
    /*clWaitForEvents(1, &event));
    clReleaseMemObject(mem);
    clReleaseKernel(kernel);
    clReleaseProgram(program);
    clReleaseCommandQueue(cmd_queue);*/
    clReleaseContext(context);
}

//Setup OpenCL
void OpenCLContext::Initialize(QPlainTextEdit* output)
{

    cl_int error;
    cl_uint platformIdCount = 0;

    output->appendPlainText("Setting up OpenCL:");

    //Get platform ID count
    error = clGetPlatformIDs(0, nullptr, &platformIdCount);

    if (error == CL_SUCCESS)
    {
        output->appendPlainText(QString::fromStdString("Detected OpenCL platforms: " + to_string(platformIdCount)));
    }
    else
    {
        output->appendPlainText(QString::fromStdString("Error calling clGetPlatformIDs. Error code: " + to_string(error)));
    }

    //Retrieve platform IDs
    vector<cl_platform_id> platformIds(platformIdCount);
    error = clGetPlatformIDs(platformIdCount, platformIds.data(), nullptr);

    if (error == CL_SUCCESS)
    {
        for (cl_uint i = 0; i < platformIdCount; ++i)
        {
            const char* attributeNames[4] = { "Name", "Vendor","Version", "Profile" };
            const cl_platform_info attributeTypes[4] = { CL_PLATFORM_NAME, CL_PLATFORM_VENDOR, CL_PLATFORM_VERSION, CL_PLATFORM_PROFILE };

            output->appendPlainText(QString::fromStdString("    Platform " + to_string(i + 1) + ":"));
            for (int j = 0; j < 4; ++j)
            {
                string platformInfo = "";
                size_t size;
                clGetPlatformInfo(platformIds[i], attributeTypes[j], 0, nullptr, &size);
                platformInfo.resize(size);
                clGetPlatformInfo(platformIds[i], attributeTypes[j], size, (char*)platformInfo.data(), nullptr);

                output->appendPlainText(QString::fromStdString("        " + (string)attributeNames[j] + ": " + platformInfo));
            }
        }
    }
    else
    {
        output->appendPlainText(QString::fromStdString("Error getting PlatformIDs. Error code: " + to_string(error)));
    }

    //Get device count
    cl_uint deviceIdCount = 0;
    error = clGetDeviceIDs(platformIds[0], CL_DEVICE_TYPE_ALL, 0, nullptr, &deviceIdCount);

    if (error == CL_SUCCESS)
    {
        output->appendPlainText(QString::fromStdString("Detected OpenCL devices: " + to_string(platformIds.size())));
    }
    else
    {
        output->appendPlainText(QString::fromStdString("Error calling clGetDeviceIDs. Error code: " + to_string(error)));
    }

    vector<cl_device_id> deviceIds(deviceIdCount);
    error = clGetDeviceIDs(platformIds[0], CL_DEVICE_TYPE_ALL, deviceIdCount, deviceIds.data(), nullptr);

    if (error == CL_SUCCESS)
    {
        output->appendPlainText(QString::fromStdString("OpenCL devices retrieved: " + to_string(platformIds.size())));

        for (cl_uint i = 0; i < deviceIdCount; ++i)
        {
            const char* attributeNames[4] = { "Name", "Vendor", "Version", "Driver" };
            const cl_device_info attributeTypes[4] = { CL_DEVICE_NAME, CL_DEVICE_VENDOR, CL_DEVICE_VERSION, CL_DRIVER_VERSION };

            output->appendPlainText(QString::fromStdString("    Device " + to_string(i + 1) + ":"));
            for (int j = 0; j < 4; ++j)
            {
                string deviceInfo = "";
                size_t size;
                clGetDeviceInfo(deviceIds[i], attributeTypes[j], 0, nullptr, &size);
                deviceInfo.resize(size);
                clGetDeviceInfo(deviceIds[i], attributeTypes[j], size, (char*)deviceInfo.data(), nullptr);

                output->appendPlainText(QString::fromStdString("        " + (string)attributeNames[j] + ": " + deviceInfo));
            }
        }
    }
    else
    {
        output->appendPlainText(QString::fromStdString("Error retrieving devices. Error code: " + to_string(error)));
    }

    const cl_context_properties contextProperties[] =
    {
        CL_CONTEXT_PLATFORM,
        reinterpret_cast<cl_context_properties>(platformIds[0]),
        0,0
    };

    context = clCreateContext(contextProperties, deviceIdCount, deviceIds.data(), nullptr, nullptr, &error);

    if (error == CL_SUCCESS)
    {
        output->appendPlainText(QString::fromStdString("OpenCL Context Successfully created."));
    }
    else
    {
        output->appendPlainText(QString::fromStdString("Error creating OpenCL Context. Error code: " + to_string(error)));
    }

    //Setup buffers

    //int testDataSize = 10;
    //vector<float> a = { 0,1,2,3,4,5,6,7,8,9 };
    //vector<float> b = { 9,8,7,6,5,4,3,2,1,0 };

    //aBuffer = clCreateBuffer(
    //    context,
    //    CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
    //    sizeof(float) * (testDataSize),
    //    a.data(),
    //    &error);

    //bBuffer = clCreateBuffer(
    //    context,
    //    CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
    //    sizeof(float) * (testDataSize),
    //    a.data(),
    //    &error);

    if (error == CL_SUCCESS)
    {
        output->appendPlainText(QString::fromStdString("Memory Buffers Successfully created."));
    }
    else
    {
        output->appendPlainText(QString::fromStdString("Error creating Memory Buffer Error code: " + to_string(error)));
    }

    //Read kernel program
    std::ifstream in("kernel.cl", std::ios::in | std::ios::binary);

    if (in)
    {
        string source;
        in.seekg(0, std::ios::end);
        source.resize(in.tellg());
        in.seekg(0, std::ios::beg);
        in.read(&source[0], source.size());
        in.close();

        const char* s = source.c_str();
        const char** sa = &s;


        kernelProgram = clCreateProgramWithSource(context, 1, sa, nullptr, &error);

        if (error == CL_SUCCESS)
        {
            output->appendPlainText(QString::fromStdString("Kernel Program Successfully read."));
        }
        else
        {
            output->appendPlainText(QString::fromStdString("Error reading Kernel Program Error code: " + to_string(error)));
        }
    }
    else
    {
        output->appendPlainText(QString::fromStdString("Error reading kernel file."));
    }

    //Compile Kernel Program
    error = clBuildProgram(kernelProgram, deviceIdCount, deviceIds.data(), nullptr, nullptr, nullptr);

    if (error == CL_SUCCESS)
    {
        output->appendPlainText(QString::fromStdString("Kernel Program Successfully build."));
    }
    else
    {
        // Determine the size of the log
        size_t log_size;
        clGetProgramBuildInfo(kernelProgram, deviceIds[0], CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);

        // Allocate memory for the log
        //char *log = new char[log_size];
        string log;
        log.resize(log_size);
        // Get the log
        clGetProgramBuildInfo(kernelProgram, deviceIds[0], CL_PROGRAM_BUILD_LOG, log_size, &log[0], NULL);


        output->appendPlainText(QString::fromStdString("Error creating Kernel Program Error code: " + to_string(error)));
        output->appendPlainText(QString::fromStdString(log));
    }

    kernel = clCreateKernel(kernelProgram, "Solver", &error);

    if (error == CL_SUCCESS)
    {
        output->appendPlainText(QString::fromStdString("Kernel Successfully created."));
    }
    else
    {
        
        output->appendPlainText(QString::fromStdString("Error creating Kernel Error code: " + to_string(error)));
    }


    queue = clCreateCommandQueue(context, deviceIds[0], 0, &error);

    if (error != CL_SUCCESS)
    {
        output->appendPlainText(QString::fromStdString("Error creating command queue."));
    }



}
