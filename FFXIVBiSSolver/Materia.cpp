#include "stdafx.h"
#include "Materia.h"
#include "GearItem.h"
#include "StatWeights.h"

Materia::Materia()
{
}


Materia::~Materia()
{
}

void Materia::AddCombosForItem(GearItem& item, StatWeights statWeights)
{
    std::vector<std::vector<MateriaTypes>> itemMateriaCombos;

    AddComboPermutation(item, statWeights, std::vector<MateriaTypes>(), itemMateriaCombos, std::vector<float>());

    //Register start and end in the combo list
    item.materiaComboCount = itemMateriaCombos.size();
    item.materiaComboIndex = allMateriaCombos.size();

    for (std::vector<MateriaTypes>& materiaCombo : itemMateriaCombos)
    {
        allMateriaCombos.push_back(EncryptMateria(materiaCombo));
    }
}

int Materia::EncryptMateria(std::vector<MateriaTypes> materiaCombo)
{
    int encryptedMateria = 0;

    for (MateriaTypes& materiaType : materiaCombo)
    {
        //Shift bits to the left to fit the materia int value
        encryptedMateria <<= 4;
        encryptedMateria += (materiaType + 1);
    }

    return encryptedMateria;
}

//Find the best materia combinations possible for this item.
//If stat caps are used we find the best scoring combinations of these capping stats.
void Materia::AddComboPermutation(GearItem& item, StatWeights statWeights, std::vector<MateriaTypes> materiaCombo, std::vector<std::vector<MateriaTypes>>& itemMateriaCombos, std::vector<float>& itemMateriaComboScores)
{
    //Materia combo filled?
    if (materiaCombo.size() == item.materiaSlots)
    {
        //Sort the materia combination
        std::sort(materiaCombo.begin(), materiaCombo.end());

        //Check caps
        if (!OneSlotOverCap(item, materiaCombo))
        {
            //Check if the combination already exists
            if (std::find(itemMateriaCombos.begin(), itemMateriaCombos.end(), materiaCombo) == itemMateriaCombos.end())
            {
                itemMateriaCombos.push_back(materiaCombo);
                itemMateriaComboScores.push_back(CalcMateriaComboScore(item, materiaCombo, statWeights));
            }
        }
    }
    else
    {
        //Keep filling materia combo
        for (int i = 0; i < materiaTypeCount; i++)
        {
            std::vector<MateriaTypes> newMateriaCombo = materiaCombo;
            newMateriaCombo.push_back(GetMateriaByIndex(i));
            AddComboPermutation(item, statWeights, newMateriaCombo, itemMateriaCombos, itemMateriaComboScores);
        }
    }
}

float Materia::CalcMateriaComboScore(GearItem& item, std::vector<MateriaTypes> materiaCombination, StatWeights statWeights)
{
    float materiaScore = 0.0f;
    //Loop through all the selected materia and add their value if under the cap, else set to the cap
    if (materiaCombination.size() > 0)
    {
        float directHit = 0;
        float criticalHitRate = 0;
        float determination = 0;
        float spellSpeed = 0;

        for (auto mType : materiaCombination)
        {
            switch (mType)
            {
            case DirectHit:
                directHit = directHit + GetMateriaValue(DirectHit) < item.directHitCap ? directHit + GetMateriaValue(DirectHit) : item.directHitCap;
                break;
            case CriticalHitRate:
                criticalHitRate = criticalHitRate + GetMateriaValue(CriticalHitRate) < item.criticalHitRateCap ? criticalHitRate + GetMateriaValue(CriticalHitRate) : item.criticalHitRateCap;
                break;
            case Determination:
                determination = determination + GetMateriaValue(Determination) < item.determinationCap ? determination + GetMateriaValue(Determination) : item.determinationCap;
                break;
            case SpellSpeed:
                spellSpeed = spellSpeed + GetMateriaValue(SpellSpeed) < item.spellSpeedCap ? spellSpeed + GetMateriaValue(SpellSpeed) : item.spellSpeedCap;
                break;
            default:
                break;
            }
        }

        materiaScore += criticalHitRate * statWeights.criticalHitRate;
        materiaScore += determination * statWeights.determination;
        materiaScore += spellSpeed * statWeights.spellSpeed;
    }
    return materiaScore;
}

bool Materia::OneSlotOverCap(GearItem& item, std::vector<MateriaTypes> materiaCombo)
{
    float intelligence = 0.0f;
    float directHit = 0.0f;
    float criticalHitRate = 0.0f;
    float determination = 0.0f;
    float skillSpeed = 0.0f;
    float spellSpeed = 0.0f;
    float parry = 0.0f;
    float vitality = 0.0f;
    float piety = 0.0f;

    //Calculate materia sums
    for (MateriaTypes materiaType : materiaCombo)
    {
        switch (materiaType)
        {
        case Intelligence:
            intelligence += GetMateriaValue(Intelligence);
            break;
        case DirectHit:
            directHit += GetMateriaValue(DirectHit);
            break;
        case CriticalHitRate:
            criticalHitRate += GetMateriaValue(CriticalHitRate);
            break;
        case Determination:
            determination += GetMateriaValue(Determination);
            break;
        case SkillSpeed:
            skillSpeed += GetMateriaValue(SkillSpeed);
            break;
        case SpellSpeed:
            spellSpeed += GetMateriaValue(SpellSpeed);
            break;
        case Parry:
            parry += GetMateriaValue(Parry);
            break;
        case Vitality:
            vitality += GetMateriaValue(Vitality);
            break;
        case Piety:
            piety += GetMateriaValue(Piety);
            break;
        }
    }

    //If a stat is over the cap by 1 or more slots return true
    if ((intelligence - item.intelligenceCap) >= GetMateriaValue(Intelligence)) return true;
    if ((directHit - item.directHitCap) >= GetMateriaValue(DirectHit)) return true;
    if ((criticalHitRate - item.criticalHitRateCap) >= GetMateriaValue(CriticalHitRate)) return true;
    if ((determination - item.determinationCap) >= GetMateriaValue(Determination)) return true;
    if ((skillSpeed - item.skillSpeedCap) >= GetMateriaValue(SkillSpeed)) return true;
    if ((spellSpeed - item.spellSpeedCap) >= GetMateriaValue(SpellSpeed)) return true;
    if ((parry - item.parryCap) >= GetMateriaValue(Parry)) return true;
    if ((vitality - item.vitalityCap) >= GetMateriaValue(Vitality)) return true;
    if ((piety - item.pietyCap) >= GetMateriaValue(Piety)) return true;

    return false;
}

Materia::MateriaTypes Materia::GetMateriaByIndex(int i)
{
    switch (i)
    {
    case(0):
        return Intelligence;
    case(1):
        return DirectHit;
    case(2):
        return CriticalHitRate;
    case(3):
        return Determination;
    case(4):
        return SkillSpeed;
    case(5):
        return SpellSpeed;
    case(6):
        return Parry;
    case(7):
        return Vitality;
    case(8):
        return Piety;
    default:
        throw std::out_of_range("Invalid materia index called.");
    }
}

//int Materia::GetMateriaIndex(MateriaTypes materiaType)
//{
//    switch (materiaType)
//    {
//    case Intelligence:
//        return 0;
//        break;
//    case DirectHit:
//        return 1;
//        break;
//    case CriticalHitRate:
//        return 2;
//        break;
//    case Determination:
//        return 3;
//        break;
//    case SkillSpeed:
//        return 12;
//        break;
//    case SpellSpeed:
//        return 12;
//        break;
//    case Parry:
//        return 12;
//        break;
//    case Vitality:
//        return 15;
//        break;
//    case Piety:
//        return 15;
//        break;
//    default:
//        throw std::out_of_range("Invalid materia type called.");
//    }
//}

int Materia::GetMateriaValue(MateriaTypes materiaType)
{
    switch (materiaType)
    {
    case Intelligence:
        return 15;
        break;
    case DirectHit:
        return 12;
        break;
    case CriticalHitRate:
        return 12;
        break;
    case Determination:
        return 12;
        break;
    case SkillSpeed:
        return 12;
        break;
    case SpellSpeed:
        return 12;
        break;
    case Parry:
        return 12;
        break;
    case Vitality:
        return 15;
        break;
    case Piety:
        return 15;
        break;
    default:
        throw std::out_of_range("Invalid materia type called.");
    }
}

void Materia::RemoveCombosForItem(GearItem& item, StatWeights statWeights)
{

}