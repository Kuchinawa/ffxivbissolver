#pragma once

typedef struct GearItem
{
    cl_float weaponDMG;
    cl_float intelligence;
    cl_float directHit;
    cl_float criticalHitRate;
    cl_float determination;
    cl_float skillSpeed;
    cl_float spellSpeed;
    cl_float parry;
    cl_float vitality;
    cl_float piety;
    cl_float intelligenceCap;
    cl_float directHitCap;
    cl_float criticalHitRateCap;
    cl_float determinationCap;
    cl_float skillSpeedCap;
    cl_float spellSpeedCap;
    cl_float parryCap;
    cl_float vitalityCap;
    cl_float pietyCap;
    cl_int materiaSlots;
    cl_int materiaComboIndex;
    cl_int materiaComboCount;
} GearItem;