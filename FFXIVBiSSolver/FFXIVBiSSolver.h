#pragma once

#include <QtWidgets/QWidget>
#include "ui_FFXIVBiSSolver.h"
#include "OpenCLContext.h"

class FFXIVBiSSolver : public QWidget
{
    Q_OBJECT

public:
    FFXIVBiSSolver(QWidget *parent = Q_NULLPTR);

    void InitializeOpenCL();

private:
    Ui::FFXIVBiSSolverClass ui;

    OpenCLContext CLContext;

    private slots:
    void on_solveButton_clicked();
};
