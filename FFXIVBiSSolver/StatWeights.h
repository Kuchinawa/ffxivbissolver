#pragma once

typedef struct StatWeights
{
    cl_float weaponDMG;
    cl_float intelligence;
    cl_float directHit;
    cl_float criticalHitRate;
    cl_float determination;
    cl_float skillSpeed;
    cl_float spellSpeed;
    cl_float parry;
    cl_float vitality;
    cl_float piety;
} StatWeights;