#pragma once
class OpenCLContext
{
public:
    OpenCLContext();
    ~OpenCLContext();

    void Initialize(QPlainTextEdit*);

    cl_context context;

    //cl_mem aBuffer;
    //cl_mem bBuffer;

    cl_program kernelProgram;

    cl_kernel kernel;

    cl_command_queue queue;


};

