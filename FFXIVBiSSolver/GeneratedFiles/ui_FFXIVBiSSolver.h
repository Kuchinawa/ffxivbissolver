/********************************************************************************
** Form generated from reading UI file 'FFXIVBiSSolver.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FFXIVBISSOLVER_H
#define UI_FFXIVBISSOLVER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_FFXIVBiSSolverClass
{
public:
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout;
    QGridLayout *gridLayout;
    QLabel *mainstatLabel;
    QDoubleSpinBox *vitSpinBox;
    QDoubleSpinBox *spellSSpinBox;
    QLabel *parryLabel;
    QLabel *skillSLabel;
    QLabel *minLabel;
    QLabel *spellSLabel;
    QDoubleSpinBox *wdSpinBox;
    QPushButton *solveButton;
    QDoubleSpinBox *mainstatSpinBox;
    QDoubleSpinBox *skillSSpinBox;
    QLabel *wdLabel;
    QDoubleSpinBox *dhSpinBox;
    QDoubleSpinBox *parrySpinBox;
    QLabel *dhLabel;
    QSpacerItem *verticalSpacer;
    QLabel *statweightLabel;
    QSpinBox *dhminSpinBox;
    QLabel *vitLabel;
    QLabel *critLabel;
    QDoubleSpinBox *critSpinBox;
    QLabel *detLabel;
    QDoubleSpinBox *detSpinBox;
    QLabel *pieLabel;
    QDoubleSpinBox *pieSpinBox;
    QPlainTextEdit *outputText;

    void setupUi(QWidget *FFXIVBiSSolverClass)
    {
        if (FFXIVBiSSolverClass->objectName().isEmpty())
            FFXIVBiSSolverClass->setObjectName(QStringLiteral("FFXIVBiSSolverClass"));
        FFXIVBiSSolverClass->resize(613, 431);
        FFXIVBiSSolverClass->setMinimumSize(QSize(613, 312));
        layoutWidget = new QWidget(FFXIVBiSSolverClass);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 10, 591, 411));
        horizontalLayout = new QHBoxLayout(layoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setHorizontalSpacing(6);
        gridLayout->setContentsMargins(-1, 0, 0, -1);
        mainstatLabel = new QLabel(layoutWidget);
        mainstatLabel->setObjectName(QStringLiteral("mainstatLabel"));
        mainstatLabel->setLayoutDirection(Qt::LeftToRight);
        mainstatLabel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(mainstatLabel, 2, 0, 1, 1);

        vitSpinBox = new QDoubleSpinBox(layoutWidget);
        vitSpinBox->setObjectName(QStringLiteral("vitSpinBox"));
        vitSpinBox->setDecimals(3);
        vitSpinBox->setSingleStep(0.001);

        gridLayout->addWidget(vitSpinBox, 9, 1, 1, 1);

        spellSSpinBox = new QDoubleSpinBox(layoutWidget);
        spellSSpinBox->setObjectName(QStringLiteral("spellSSpinBox"));
        spellSSpinBox->setDecimals(3);
        spellSSpinBox->setSingleStep(0.001);
        spellSSpinBox->setValue(0.195);

        gridLayout->addWidget(spellSSpinBox, 7, 1, 1, 1);

        parryLabel = new QLabel(layoutWidget);
        parryLabel->setObjectName(QStringLiteral("parryLabel"));
        parryLabel->setLayoutDirection(Qt::LeftToRight);
        parryLabel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(parryLabel, 8, 0, 1, 1);

        skillSLabel = new QLabel(layoutWidget);
        skillSLabel->setObjectName(QStringLiteral("skillSLabel"));
        skillSLabel->setLayoutDirection(Qt::LeftToRight);
        skillSLabel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(skillSLabel, 6, 0, 1, 1);

        minLabel = new QLabel(layoutWidget);
        minLabel->setObjectName(QStringLiteral("minLabel"));
        minLabel->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(minLabel, 0, 2, 1, 1);

        spellSLabel = new QLabel(layoutWidget);
        spellSLabel->setObjectName(QStringLiteral("spellSLabel"));
        spellSLabel->setLayoutDirection(Qt::LeftToRight);
        spellSLabel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(spellSLabel, 7, 0, 1, 1);

        wdSpinBox = new QDoubleSpinBox(layoutWidget);
        wdSpinBox->setObjectName(QStringLiteral("wdSpinBox"));
        wdSpinBox->setDecimals(3);
        wdSpinBox->setSingleStep(0.001);
        wdSpinBox->setValue(11.835);

        gridLayout->addWidget(wdSpinBox, 1, 1, 1, 1);

        solveButton = new QPushButton(layoutWidget);
        solveButton->setObjectName(QStringLiteral("solveButton"));

        gridLayout->addWidget(solveButton, 11, 0, 1, 3);

        mainstatSpinBox = new QDoubleSpinBox(layoutWidget);
        mainstatSpinBox->setObjectName(QStringLiteral("mainstatSpinBox"));
        mainstatSpinBox->setDecimals(3);
        mainstatSpinBox->setSingleStep(0.001);
        mainstatSpinBox->setValue(1);

        gridLayout->addWidget(mainstatSpinBox, 2, 1, 1, 1);

        skillSSpinBox = new QDoubleSpinBox(layoutWidget);
        skillSSpinBox->setObjectName(QStringLiteral("skillSSpinBox"));
        skillSSpinBox->setDecimals(3);
        skillSSpinBox->setSingleStep(0.001);
        skillSSpinBox->setValue(0);

        gridLayout->addWidget(skillSSpinBox, 6, 1, 1, 1);

        wdLabel = new QLabel(layoutWidget);
        wdLabel->setObjectName(QStringLiteral("wdLabel"));
        wdLabel->setLayoutDirection(Qt::LeftToRight);
        wdLabel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(wdLabel, 1, 0, 1, 1);

        dhSpinBox = new QDoubleSpinBox(layoutWidget);
        dhSpinBox->setObjectName(QStringLiteral("dhSpinBox"));
        dhSpinBox->setDecimals(3);
        dhSpinBox->setSingleStep(0.001);

        gridLayout->addWidget(dhSpinBox, 3, 1, 1, 1);

        parrySpinBox = new QDoubleSpinBox(layoutWidget);
        parrySpinBox->setObjectName(QStringLiteral("parrySpinBox"));
        parrySpinBox->setDecimals(3);
        parrySpinBox->setSingleStep(0.001);

        gridLayout->addWidget(parrySpinBox, 8, 1, 1, 1);

        dhLabel = new QLabel(layoutWidget);
        dhLabel->setObjectName(QStringLiteral("dhLabel"));
        dhLabel->setLayoutDirection(Qt::LeftToRight);
        dhLabel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(dhLabel, 3, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 12, 0, 1, 3);

        statweightLabel = new QLabel(layoutWidget);
        statweightLabel->setObjectName(QStringLiteral("statweightLabel"));
        statweightLabel->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(statweightLabel, 0, 0, 1, 2);

        dhminSpinBox = new QSpinBox(layoutWidget);
        dhminSpinBox->setObjectName(QStringLiteral("dhminSpinBox"));
        dhminSpinBox->setMaximum(999999);
        dhminSpinBox->setValue(592);

        gridLayout->addWidget(dhminSpinBox, 3, 2, 1, 1);

        vitLabel = new QLabel(layoutWidget);
        vitLabel->setObjectName(QStringLiteral("vitLabel"));
        vitLabel->setLayoutDirection(Qt::LeftToRight);
        vitLabel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(vitLabel, 9, 0, 1, 1);

        critLabel = new QLabel(layoutWidget);
        critLabel->setObjectName(QStringLiteral("critLabel"));
        critLabel->setLayoutDirection(Qt::LeftToRight);
        critLabel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(critLabel, 4, 0, 1, 1);

        critSpinBox = new QDoubleSpinBox(layoutWidget);
        critSpinBox->setObjectName(QStringLiteral("critSpinBox"));
        critSpinBox->setDecimals(3);
        critSpinBox->setSingleStep(0.001);
        critSpinBox->setValue(0.287);

        gridLayout->addWidget(critSpinBox, 4, 1, 1, 1);

        detLabel = new QLabel(layoutWidget);
        detLabel->setObjectName(QStringLiteral("detLabel"));
        detLabel->setLayoutDirection(Qt::LeftToRight);
        detLabel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(detLabel, 5, 0, 1, 1);

        detSpinBox = new QDoubleSpinBox(layoutWidget);
        detSpinBox->setObjectName(QStringLiteral("detSpinBox"));
        detSpinBox->setDecimals(3);
        detSpinBox->setSingleStep(0.001);
        detSpinBox->setValue(0.192);

        gridLayout->addWidget(detSpinBox, 5, 1, 1, 1);

        pieLabel = new QLabel(layoutWidget);
        pieLabel->setObjectName(QStringLiteral("pieLabel"));
        pieLabel->setLayoutDirection(Qt::LeftToRight);
        pieLabel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(pieLabel, 10, 0, 1, 1);

        pieSpinBox = new QDoubleSpinBox(layoutWidget);
        pieSpinBox->setObjectName(QStringLiteral("pieSpinBox"));
        pieSpinBox->setDecimals(3);
        pieSpinBox->setSingleStep(0.001);

        gridLayout->addWidget(pieSpinBox, 10, 1, 1, 1);


        horizontalLayout->addLayout(gridLayout);

        outputText = new QPlainTextEdit(layoutWidget);
        outputText->setObjectName(QStringLiteral("outputText"));
        outputText->setEnabled(true);
        outputText->setReadOnly(true);

        horizontalLayout->addWidget(outputText);

        QWidget::setTabOrder(wdSpinBox, mainstatSpinBox);
        QWidget::setTabOrder(mainstatSpinBox, dhSpinBox);
        QWidget::setTabOrder(dhSpinBox, skillSSpinBox);
        QWidget::setTabOrder(skillSSpinBox, spellSSpinBox);
        QWidget::setTabOrder(spellSSpinBox, parrySpinBox);

        retranslateUi(FFXIVBiSSolverClass);

        QMetaObject::connectSlotsByName(FFXIVBiSSolverClass);
    } // setupUi

    void retranslateUi(QWidget *FFXIVBiSSolverClass)
    {
        FFXIVBiSSolverClass->setWindowTitle(QApplication::translate("FFXIVBiSSolverClass", "FFXIVBiSSolver", Q_NULLPTR));
        mainstatLabel->setText(QApplication::translate("FFXIVBiSSolverClass", "Main Stat:", Q_NULLPTR));
        parryLabel->setText(QApplication::translate("FFXIVBiSSolverClass", "Parry:", Q_NULLPTR));
        skillSLabel->setText(QApplication::translate("FFXIVBiSSolverClass", "Skill Speed:", Q_NULLPTR));
        minLabel->setText(QApplication::translate("FFXIVBiSSolverClass", "Min:", Q_NULLPTR));
        spellSLabel->setText(QApplication::translate("FFXIVBiSSolverClass", "Spell Speed:", Q_NULLPTR));
        solveButton->setText(QApplication::translate("FFXIVBiSSolverClass", "Solve", Q_NULLPTR));
        wdLabel->setText(QApplication::translate("FFXIVBiSSolverClass", "Weapon Damage:", Q_NULLPTR));
        dhLabel->setText(QApplication::translate("FFXIVBiSSolverClass", "Direct Hit", Q_NULLPTR));
        statweightLabel->setText(QApplication::translate("FFXIVBiSSolverClass", "Stat Weights:", Q_NULLPTR));
        vitLabel->setText(QApplication::translate("FFXIVBiSSolverClass", "Vitality:", Q_NULLPTR));
        critLabel->setText(QApplication::translate("FFXIVBiSSolverClass", "Critical Hit Rate:", Q_NULLPTR));
        detLabel->setText(QApplication::translate("FFXIVBiSSolverClass", "Determination:", Q_NULLPTR));
        pieLabel->setText(QApplication::translate("FFXIVBiSSolverClass", "Piety:", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class FFXIVBiSSolverClass: public Ui_FFXIVBiSSolverClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FFXIVBISSOLVER_H
